# Kerkajou

## Notes

* [Diamond-square algorithm](https://en.wikipedia.org/wiki/Diamond-square_algorithm) (!w.fr)

## Tool

* [lite-server](https://www.npmjs.com/package/lite-server)

## Problem

* [On npm install unhandled rejection error eacces permission denied](https://stackoverflow.com/questions/50639690/on-npm-install-unhandled-rejection-error-eacces-permission-denied)

## Command

At initialization :

```sh
npm install -g json-server
npm init -y
npm install
```

To launch the application :

```sh
npm run all
```

* [MDN](https://developer.mozilla.org/fr/docs/Web/Guide/AJAX)
* [Tutoriel Ajax](https://www.xul.fr/xml-ajax.html)
* [Fetch API requesting multiple get requests](https://stackoverflow.com/questions/46241827/fetch-api-requesting-multiple-get-requests)
* [Utiliser Fetch](https://developer.mozilla.org/fr/docs/Web/API/Fetch_API/Using_Fetch)
* [fetch : La fin de $.ajax()](https://blog.nathanaelcherrier.com/fr/fetch-fin-jquery-ajax/)
* [Pour la création d'un tableau à la volée](https://stackoverflow.com/questions/6299500/tersest-way-to-create-an-array-of-integers-from-1-20-in-javascript)
* [Array with map](https://zellwk.com/blog/async-await-in-loops/)
* [async/await with fetch and map](https://gist.github.com/ericls/f11d58b69faa236883fc5c0249b315dc) : Ressource retenue !
* [JSON Server](https://github.com/typicode/json-server)
