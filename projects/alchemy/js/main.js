// Exemple de sortie, et non de l'entrée
[
    {
        "name": "bark",
        "native-condition": "solid",
        "effect": "heal",
        "side-effect": "unstable",
        "transformation" : [
            {
                "process": "grinding",
                "name": "bark powder",
                "processing-difficulty": 3
            },
            "drying"
        ],
        "rarity": 5,
        "price": 10,
        "location": "forest"
    }
]