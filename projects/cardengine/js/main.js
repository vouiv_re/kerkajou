"use strict";

// import html2canvas from 'html2canvas';

let main = document.getElementById("main");
let nav = document.getElementById("nav");
let download = document.getElementById("download");

let spans = document.getElementsByTagName("img");
let validate = document.getElementById("validate");

let name = document.getElementById("name");
let inputName = document.getElementById("inputName");
let people = document.getElementById("people");
let inputPeople = document.getElementById("inputPeople");
let dicePeople = document.getElementById("dicePeople");

let pou = document.getElementById("card-framework");

let peoples = [];

download.addEventListener("mouseup", () => {
    // var doc = new jsPDF()
    // doc.text('Hello world!', 10, 10)
    // doc.save('a4.pdf')

    // var doc = new jsPDF();
    // var source = window.document.getElementById("card-framework");
    // doc.fromHTML(source);
    // doc.output("dataurlnewwindow");

    // html2canvas(document.getElementById("card-framework")).then(canvas => {
    //     document.body.appendChild(canvas);
    // });
    // html2canvas(document.body).then(function(canvas) {
    //     // Export the canvas to its data URI representation
    //     var base64image = canvas.toDataURL("image/png");
    
    //     // Open the image in a new window
    //     window.open(base64image , "_blank");
    // });

    html2canvas(pou).then(function(canvas) {
        // Export the canvas to its data URI representation
        var base64image = canvas.toDataURL("image/png");
    
        // Open the image in a new window
        window.open(base64image , "_blank");
    });
}, false);

window.onload = function () {
    fetch('./data/people.json')
        .then(response => {
            return response.json();
        })
        .then(data => {
            main.style.display = "flex";
            nav.style.display = "flex";
            peoples = data;
        })
        .catch(err => {
        })
}

for (let i = 0; i < spans.length; i++) {
    spans[i].addEventListener("mouseup", () => {
        spans[i].animate([
            { transform: 'rotate(0deg)' },
            { transform: 'rotate(360deg)' }
        ], {
            duration: 300
        });
        validation();
    }, false);
}

dicePeople.addEventListener("click", () => {
    inputPeople.value = peoples[getRandom(peoples.length)].name;
    createName();
    validation();
}, false);

validate.addEventListener("click", () => {
    validation();
}, false);

function validation() {
    let n = inputName.value;
    let p = inputPeople.value;
    if (n != undefined && n != '') name.innerHTML = toCapitalize(n);
    if (p != undefined && p != '') people.innerHTML = toCapitalize(p);
}

function toCapitalize(input) {
    return `${input[0].toUpperCase()}${input.slice(1)}`;
}

function getRandom(max, min = 0) {
    return Math.floor(Math.random() * (Math.floor(max - 1) - Math.ceil(min) + 1)) + Math.ceil(min);
}

function createName() {
    let syllable = peoples[0]["subrace"][0]["gender"][0];
    let name = '';
    for (let i = 0; i < syllable["length"][getRandom(syllable["length"].length)]; i++) {
        name += syllable["syllable"][getRandom(syllable["syllable"].length)];
    }
    inputName.value = name;
}