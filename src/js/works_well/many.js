"use strict";

// Done
// The purpose is to inform in the console three differents pokemon names when clicking

let i = 1;
let url = `https://pokeapi.co/api/v2/pokemon/`;
let buttonMany = document.getElementById('buttonMany');

async function asyncCall(url) {
    let [poke1, poke2, poke3] = await Promise.all([
        fetch(`${url}${i}/`),
        fetch(`${url}${i + 1}/`),
        fetch(`${url}${i + 2}/`)
    ]);
    let data1 = await poke1.json();
    let data2 = await poke2.json();
    let data3 = await poke3.json();

    return `${data1.name} - ${data2.name} - ${data3.name}`
};

buttonMany.addEventListener("click", function () {
    asyncCall(url)
        .then(pokemons => console.info(pokemons))
    i += 3;
}, false);