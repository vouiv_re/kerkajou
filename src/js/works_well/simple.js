"use strict";

// Done
// Just inform in the console when the request is done a pokemon name

async function asyncCall() {
    let response = await fetch('https://pokeapi.co/api/v2/pokemon/1/');
    let data = await response.json();

    return data;
};

asyncCall()
    .then(pokemon => console.info(pokemon.name))
