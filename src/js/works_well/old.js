"use strict";

// Done
// The purpose is to inform in the console a pokemon name with old method

let oldButton = document.getElementById('oldButton');

function requestListener() {
    let data = JSON.parse(this.responseText);
    console.info(data.name);
}

function requestError(error) {
    console.error('We are in trouble...', error);
}

let request = new XMLHttpRequest();
request.onload = requestListener;
request.onerror = requestError;

oldButton.addEventListener("click", function () {
    request.open('get', 'https://pokeapi.co/api/v2/pokemon/1/', true);
    request.send();
}, false);