"use strict";

// Done
// The button 'Fetch' has to inform in the console a different pokemon name at each click

let i = 1;
let url = `https://pokeapi.co/api/v2/pokemon/`;
let button = document.getElementById('fetch');

async function asyncCall(url) {
    let response = await fetch(url);
    let data = await response.json();

    return data;
};

button.addEventListener("click", function () {
    asyncCall(`${url}${i}/`)
        .then(pokemon => console.info(pokemon.name))
    i++;
}, false);