// export const sendFetch = async () =>

// async function assyncCall() {
//     try {
//         let poke1 = await Promise.all([
//             fetch("https://pokeapi.co/api/v2/pokemon/1/")
//         ])
//         // JSON.parse(poke1);

//         // console.log(`${data1.name}`);

//     }
//     catch (err) {
//         console.log(err);
//     };

// };

// fetchAsync()
//     .then(data => console.log(data))
//     .catch(reason => console.log(reason.message))

async function assyncCall() {
    let response = await fetch("https://pokeapi.co/api/v2/pokemon/1/")
    let data = await response.json();
    return data.name;
};

assyncCall()
    // .then(poke1 => JSON.parse(poke1))
    .then(pokemon => console.log(pokemon))

// fetch(`https://pokeapi.co/api/v2/pokemon/pikachu/')
//     .then(response => response.json() )
//     .then(pokemon => console.log(pokemon) )

// async function getPage(url) {
//     let res = await fetch(url)
//     let text = await res.text()
//     return text
//   }


// async function assyncCall() {
    // try {
        // let [poke1, poke2, poke3] = await Promise.all([
            // fetch("https://pokeapi.co/api/v2/pokemon/1/"),
            // fetch("https://pokeapi.co/api/v2/pokemon/2/"),
            // fetch("https://pokeapi.co/api/v2/pokemon/3/")
        // ]);
        // let data1 = JSON.parse(poke1);
        // let data2 = JSON.parse(poke2);
        // let data3 = JSON.parse(poke3);
        // console.log(`${ data1.name } - ${ data2.name } - ${ data3.name }`);
        // console.log(`${ data1.name }`);
// 
    // }
    // catch (err) {
        // console.log(err);
    // };
// 
// };
// 
// assyncCall();

// let fetch = document.getElementById('fetch');

// fetch.addEventListener("click", function () {
//     console.log(`test`);

//     sendFetch();
// }, false);