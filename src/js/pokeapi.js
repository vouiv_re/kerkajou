// let random = Math.floor(Math.random() * Math.floor(max));

// WIP

// To do
// - The reverse pagination
// - The link on elements in pagination to select pokemon and make it appear

let pokemonNb = 25;

let main = document.getElementById('main');
let section = document.getElementById('section');
let pokeName = document.createElement('p');


let number = 1;
let url = `https://pokeapi.co/api/v2/pokemon/`;
let getPokemon = document.getElementsByClassName('getPokemon');

function inputValue() {
    return parseInt(document.getElementById('inputValue').value);
}

async function asyncCall() {
    let result = Array.from(Array(inputValue()), (x, i) => i).map(i => i + number);

    const data = Promise.all(
        result.map(async (i) => await (await fetch(`${url}${i}`)).json())
    )
    return data;
}

for (let i = 0; i < getPokemon.length; i++) {
    getPokemon[i].addEventListener("click", function () {
        section.innerHTML = '';
        asyncCall()
            .then(data => {
                data.forEach(pokemon => {
                    let pokeImg = document.createElement('img');
                    pokeImg.src = pokemon.sprites.front_default;
                    section.appendChild(pokeImg);
                    console.info(`${pokemon.name}`);
                });
            })
        number += inputValue();
    }, false);
}